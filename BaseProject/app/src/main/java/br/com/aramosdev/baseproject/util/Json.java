package br.com.aramosdev.baseproject.util;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

//
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.util.EntityUtils;

/**
 * Created by programador on 14/01/15.
 * @author Alberto Ramos
 */
public abstract class Json{

    private static Class[] noparams = {};
    private static Class[] param = new Class[1];


    public static AsyncTask<String, Void, String> httpAsyncTask(final Object objectReturn,
                                                                final Object obj,
                                                                final String nameMethodExecFinalPost){
        return new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... urls) {
                return Json.POST(urls[0], obj);
            }
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    Json.param[0] = String.class;
                    Method method = objectReturn.getClass().getMethod(nameMethodExecFinalPost,Json.param);
                    method.invoke(objectReturn,result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
    public static String POST(String url, Object obj)  {
//        String result = null;
//        Log.d("Url", url);
//        long startTime = System.currentTimeMillis();
//        HttpClient httpclient = new DefaultHttpClient();
//        HttpPost httpPost = new HttpPost(url);
//        List<NameValuePair> params =  new ArrayList<>();
//        Log.d("Url", obj.getClass().getName());
//        Field[] fields = obj.getClass().getDeclaredFields();
//        try {
//            for (int i = 0; i < fields.length ; i++) {
//                Method method = null;
//                method = obj.getClass()
//                        .getDeclaredMethod("get" + Util.firstUpperCase(fields[i].getName()), Json.noparams);
//                params.add(new BasicNameValuePair(fields[i].getName(), String.valueOf(method.invoke(obj, null))));
//                Log.d(params.get(i).getName(), params.get(i).getValue());
//            }
//            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
//            HttpResponse httpResponse = httpclient.execute(httpPost);
//            HttpEntity ent = httpResponse.getEntity();
//            result = EntityUtils.toString(ent);
//        } catch (Exception e ) {
//            Log.e("erro connecção :", e.getMessage());
//            e.printStackTrace();
//        }finally {
//            long elapsedTime = System.currentTimeMillis() - startTime;
//            Log.i("Total request: ", String.valueOf(elapsedTime));
//            return result;
//        }
        return null;
    }
    public static String readJsonString(String json,String field) throws JSONException {
        JSONObject mainObject = new JSONObject(json);
        return mainObject.getString(field).toString();
    }
}
