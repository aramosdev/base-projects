package br.com.aramosdev.baseproject.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import br.com.aramosdev.baseproject.DAO.ManagerDataSource;
import br.com.aramosdev.baseproject.entity.RespostaPesquisa;

/**
 * Created by programador on 23/03/15.
 */
public class ManageRespostaPesquisa {
    private ManagerDataSource dataSource;
    private Context context;
    private RespostaPesquisa respostaPesquisa;

    public ManageRespostaPesquisa(Context context) {
        this.context = context;
        if (this.dataSource == null)
            this.dataSource = new ManagerDataSource(this.context);
        if (this.respostaPesquisa == null)
            this.respostaPesquisa = new RespostaPesquisa();
    }

    public void insertRespostaPesquisa(RespostaPesquisa respostaPesquisa){
        this.respostaPesquisa = respostaPesquisa;
        this.dataSource.open();
        try {
            this.dataSource.insert(this.respostaPesquisa);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
    }
    public List<RespostaPesquisa> getListRespostasPesquisas(){
        this.dataSource.open();
        List<RespostaPesquisa> respostaPesquisas = new ArrayList<>();
        try {
            respostaPesquisas = (List<RespostaPesquisa>) this.dataSource.getAll(this.respostaPesquisa);
            if (respostaPesquisas == null)
                respostaPesquisas = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return respostaPesquisas;
    }
    public boolean delete(RespostaPesquisa respostaPesquisa){
        this.dataSource.open();
        try {
            this.dataSource.deleteRow(respostaPesquisa);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return true;
    }
}