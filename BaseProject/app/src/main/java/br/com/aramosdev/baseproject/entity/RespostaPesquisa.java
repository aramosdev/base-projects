package br.com.aramosdev.baseproject.entity;

import java.io.Serializable;

import br.com.aramosdev.baseproject.annotation.Autoincrement;
import br.com.aramosdev.baseproject.annotation.Field;
import br.com.aramosdev.baseproject.annotation.Id;
import br.com.aramosdev.baseproject.annotation.Table;

/**
 * @author Alberto Ramos
 * Created by programador on 23/03/15.
 */
@Table(name = "respostas_pesquisas")
public class RespostaPesquisa implements Serializable {
    @Id
    @Autoincrement
    @Field(name="resposta_pergunta_id", type="integer")
    private int resposta_pergunta_id;
    @Field(name="pergunta",type="text")
    private String pergunta;
    @Field(name="pesquisa_id",type="int")
    private int pesquisa_id;
    @Field(name="token", type="text")
    private String token;

    public int getResposta_pergunta_id() {
        return resposta_pergunta_id;
    }
    public void setResposta_pergunta_id(int resposta_pergunta_id) {
        this.resposta_pergunta_id = resposta_pergunta_id;
    }
    public String getPergunta() {
        return pergunta;
    }
    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }
    public int getPesquisa_id() {
        return pesquisa_id;
    }
    public void setPesquisa_id(int pesquisa_id) {
        this.pesquisa_id = pesquisa_id;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
}
