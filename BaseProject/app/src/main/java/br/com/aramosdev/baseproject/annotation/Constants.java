package br.com.aramosdev.baseproject.annotation;

/**
 * @author Alberto Ramos
 * Created by programador on 28/01/15.
 */
public @interface Constants {
    String URL_LOGIN_USER = "http://painel.opineonline.com.br/webservice/auth";
    String URL_GET_PESQUISAS = "http://painel.opineonline.com.br/webservice/pesquisas";
    String URL_GET_PESQUISAS_FULL = "http://painel.opineonline.com.br/webservice/getListPesquisasForm";
    String URL_GET_RESPOSTA_PERGUNTA = "http://painel.opineonline.com.br/webservice/formulario";
    String URL_SET_RESPOSTA = "http://painel.opineonline.com.br/webservice/responder";
    String URL_SET_TESTE = "http://192.168.1.15/teste.php";
    final int SPLASH_TIME_OUT = 2000;
    final int RESTART_TIME_PESQUISA = 15000;
    final String MINE_HTML = "text/html";
    final String ENCONDING = "utf-8";
    static final int SENDS_RESPOSTAS_TIME_OUT = 5000;
}
