package br.com.aramosdev.baseproject.model;

import android.util.Log;

import java.lang.reflect.Method;
import java.util.List;

import br.com.aramosdev.baseproject.annotation.Constants;
import br.com.aramosdev.baseproject.entity.Pesquisas;
import br.com.aramosdev.baseproject.util.Json;


/**
 * Alberto Ramos
 * Created by programador on 19/03/15.
 */
public class Mobile3g implements Runnable {

    private List<Pesquisas> pesquisasList;
    private ManagePesquisa managerPesquisa;
    private int fila;
    private Thread thread;
    private Object classReturn;
    private String nameMethodExecFinal;
    private Class[] param = new Class[1];
    private boolean executes;

    public Mobile3g(ManagePesquisa managerPesquisa, List<Pesquisas> pesquisasList) {
        this.managerPesquisa = managerPesquisa;
        this.pesquisasList = pesquisasList;
        this.fila = 0;
        this.executes= true;
    }
    public void start(Object obj, String nameMethodExecFinal){
        this.thread = new Thread(this);
        this.thread.start();
        this.classReturn = obj;
        this.nameMethodExecFinal = nameMethodExecFinal;
    }
    @Override
    public void run() {
        if (this.executes)
            this.executesAjax();
    }
    public void executesAjax(){
        Log.d("posição fila ", String.valueOf(this.fila));
        if (this.fila < this.pesquisasList.size()) {
            if (this.executes){
                try {
                    String result = Json.POST(Constants.URL_GET_RESPOSTA_PERGUNTA,this.pesquisasList.get(this.fila));
                    Log.d("result", result);
                    Pesquisas pesquisa = this.pesquisasList.get(this.fila);
                    pesquisa.setHtml(Json.readJsonString(result, "html"));
                    String css = Json.readJsonString(result, "css");
                    pesquisa.setBg(Json.readJsonString(css, "bg"));
                    pesquisa.setLogo_base64(Json.readJsonString(css, "logo_base64"));
                    pesquisa.setLogo_pesquisa(Json.readJsonString(css, "logo_pesquisa"));
                    pesquisa.setMensagem(Json.readJsonString(css, "mensagem"));
                    this.managerPesquisa.updatePesquisa(pesquisa);
                    this.param[0] = Pesquisas.class;
                    Method method = this.classReturn.getClass().getMethod(this.nameMethodExecFinal,this.param);
                    method.invoke(this.classReturn,pesquisa);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.fila++;
                this.executesAjax();
            }
        }else{
            this.thread.interrupt();
            this.executes = false;
        }
    }

}
