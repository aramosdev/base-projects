package br.com.aramosdev.baseproject.entity;

import java.io.Serializable;

import br.com.aramosdev.baseproject.annotation.Field;
import br.com.aramosdev.baseproject.annotation.Id;
import br.com.aramosdev.baseproject.annotation.Table;

/**
 * @author Alberto Ramos
 * Created by programador on 04/03/15.
 */
@Table(name="pesquisas")
public class Pesquisas implements Serializable {
    @Id
    @Field(name="pesquisa_id",type = "integer")
    private int pesquisa_id;
    @Field(name="nome",type = "text")
    private String nome;
    @Field(name="cadastro",type = "text")
    private String cadastro;
    @Field(name="token",type = "text")
    private String token;
    @Field(name="bg",type = "text")
    private String bg;
    @Field(name="color",type = "text")
    private String color;
    @Field(name="logo_pesquisa",type = "text")
    private String logo_pesquisa;
    @Field(name="logo_base64",type = "text")
    private String logo_base64;
    @Field(name="html",type = "text")
    private String html;
    @Field(name = "mensagem",type = "text")
    private String mensagem;

    public int getPesquisa_id() {
        return pesquisa_id;
    }
    public void setPesquisa_id(int pesquisa_id) {
        this.pesquisa_id = pesquisa_id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCadastro() {
        return cadastro;
    }
    public void setCadastro(String cadastro) {
        this.cadastro = cadastro;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getBg() {
        return bg;
    }
    public void setBg(String bg) {
        this.bg = bg;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getLogo_pesquisa() {
        return logo_pesquisa;
    }
    public void setLogo_pesquisa(String logo_pesquisa) {
        this.logo_pesquisa = logo_pesquisa;
    }
    public String getLogo_base64() {
        return logo_base64;
    }
    public void setLogo_base64(String logo_base64) {
        this.logo_base64 = logo_base64;
    }
    public String getHtml() {
        return html;
    }
    public void setHtml(String html) {
        this.html = html;
    }
    public String getMensagem() {
        return mensagem;
    }
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
