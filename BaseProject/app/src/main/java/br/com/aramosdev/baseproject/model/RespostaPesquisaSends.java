package br.com.aramosdev.baseproject.model;

import android.content.Context;
import android.util.Log;

import java.util.List;

import br.com.aramosdev.baseproject.annotation.Constants;
import br.com.aramosdev.baseproject.entity.RespostaPesquisa;
import br.com.aramosdev.baseproject.util.ConnectionInternet;
import br.com.aramosdev.baseproject.util.Json;


/**
 * @author Alberto Ramos
 * Created by programador on 24/03/15.
 */
public class RespostaPesquisaSends implements Runnable {
    private boolean executes;
    private int fila;
    private Context context;
    private List<RespostaPesquisa> filaRespostaPesquisa;
    private ManageRespostaPesquisa respostaPesquisa;
    private Thread thread;

    public RespostaPesquisaSends(Context context) {
        this.context = context;
        this.executes = false;
        this.fila = 0;
        if (this.respostaPesquisa == null){
            this.respostaPesquisa = new ManageRespostaPesquisa(this.context);
        }
    }
    public void start() {
        if (this.checkFila()){
            Log.d("fila ===", String.valueOf(this.fila));
            this.thread = new Thread(this);
            this.thread.start();
        }
    }
    @Override
    public void run() {
        Log.d("isExecutes ===", String.valueOf(this.isExecutes()));
        if (this.isExecutes()){
            this.manageSendsRespostas();
        }
    }
    public boolean isExecutes() {
        return this.executes;
    }
    public void setExecutes(boolean executes) {
        this.executes = executes;
    }
    private boolean checkFila(){
        this.filaRespostaPesquisa = this.respostaPesquisa.getListRespostasPesquisas();
        if (this.filaRespostaPesquisa.size() > 0){
            this.fila = this.filaRespostaPesquisa.size() - 1;
            Log.d("id Respostas ===", String.valueOf(filaRespostaPesquisa.get(this.fila).getPesquisa_id()));
            this.setExecutes(true);
            return true;
        }
        this.fila = 0;
        this.setExecutes(false);
        return false;
    }
    private void manageSendsRespostas(){
        Log.d("fila SendsRespostas ===", String.valueOf(this.fila));
        Log.d("isExecutes Respostas ", String.valueOf(this.isExecutes()));
        if (!this.checkFila())
            return;
        if (!ConnectionInternet.isConnection(this.context)){
            try {
                Thread.sleep(Constants.SENDS_RESPOSTAS_TIME_OUT);
                this.manageSendsRespostas();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.executesAjax();
        return;
    }
    private void executesAjax(){
        if (this.isExecutes()){
            String result = Json.POST(Constants.URL_SET_RESPOSTA,this.filaRespostaPesquisa.get(this.fila));
            int status = 0;
            if (result == null && this.filaRespostaPesquisa.size() <= 0){
                this.fila = 0;
                this.setExecutes(false);
                return;
            }else{
                try {
                    status = Integer.parseInt(Json.readJsonString(result, "status"));
                    if (status > 0)
                        this.respostaPesquisa.delete(this.filaRespostaPesquisa.get(this.fila));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.manageSendsRespostas();
            }
        }
    }
}
