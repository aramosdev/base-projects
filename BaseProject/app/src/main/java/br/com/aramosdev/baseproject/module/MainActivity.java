package br.com.aramosdev.baseproject.module;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.aramosdev.baseproject.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
