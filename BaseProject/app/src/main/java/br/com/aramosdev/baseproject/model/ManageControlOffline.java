package br.com.aramosdev.baseproject.model;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import br.com.aramosdev.baseproject.entity.Pesquisas;
import br.com.aramosdev.baseproject.entity.User;
import br.com.aramosdev.baseproject.util.Json;


/**
 * @author Alberto Ramos
 * Created by programador on 19/03/15.
 */
public class ManageControlOffline {
    private ManagePesquisa managerPesquisa;
    private Context context;
    private Mobile3g mobile3g;
    private List<Pesquisas> pesquisas;
    private User user;

    public ManageControlOffline(Context context) {
        if (this.managerPesquisa == null)
            this.managerPesquisa = new ManagePesquisa(context);
        this.context = context;
    }
    public Mobile3g saveResultPesquisas3g(String result,String token){
        try {
            Type listType = new TypeToken<ArrayList<Pesquisas>>(){}.getType();
            this.pesquisas = new Gson().fromJson(Json.readJsonString(result, "pesquisas"), listType);
            for (Pesquisas pesquisa : this.pesquisas) {
                pesquisa.setToken(token);
                this.managerPesquisa.insertPesquisa(pesquisa);
                Log.d("3g save id", String.valueOf(pesquisa.getPesquisa_id()));
                Log.d("3g save nome", pesquisa.getNome());
            }
            return new Mobile3g(this.managerPesquisa,this.pesquisas);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<Pesquisas> savePesquisasAjax(String resultAjax,String token){
        try {
            this.managerPesquisa.truncate(new Pesquisas());
            Type listType = new TypeToken<ArrayList<Pesquisas>>(){}.getType();
            this.pesquisas = new ArrayList<>();
            this.pesquisas = new Gson().fromJson(Json.readJsonString(resultAjax, "pesquisas"), listType);
            for (Pesquisas pesquisa : this.pesquisas) {
                pesquisa.setToken(token);
                this.managerPesquisa.insertPesquisa(pesquisa);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this.pesquisas;
    }
    public List<Pesquisas> getAllPesquisas(){
        return this.managerPesquisa.getListPesquisas();
    }

}
