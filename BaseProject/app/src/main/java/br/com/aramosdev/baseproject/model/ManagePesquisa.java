package br.com.aramosdev.baseproject.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import br.com.aramosdev.baseproject.DAO.ManagerDataSource;
import br.com.aramosdev.baseproject.entity.Pesquisas;

/**
 * Created by programador on 17/03/15.
 */
public class ManagePesquisa {
    private Pesquisas pesquisa;
    private ManagerDataSource dataSource;
    private Context context;

    public ManagePesquisa(Context context) {
        this.context = context;
        if (this.dataSource == null)
            this.dataSource = new ManagerDataSource(this.context);
        if (this.pesquisa == null)
            this.pesquisa = new Pesquisas();
    }
    public Pesquisas insertPesquisa(Pesquisas pesquisa){
        this.pesquisa = pesquisa;
        this.dataSource.open();
        try {
            this.dataSource.insert(this.pesquisa);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return this.pesquisa;
    }
    public List<Pesquisas> getListPesquisas(){
        this.dataSource.open();
        List<Pesquisas> listPesquisas = null;
        try {
            listPesquisas = (List<Pesquisas>) this.dataSource.getAll(this.pesquisa);
            if (listPesquisas == null)
                listPesquisas = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return listPesquisas;
    }
    public void updatePesquisa(Pesquisas pesquisa){
        this.dataSource.open();
        try {
            this.dataSource.updateValuesObject(pesquisa);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
    }
    public boolean truncate(Pesquisas pesquisa){
        this.dataSource.open();
        this.dataSource.truncate(pesquisa);
        this.dataSource.close();
        return true;
    }
    public boolean isPesquisa(int pesquisa_id){
        this.dataSource.open();
        try {
            if (this.dataSource.getObjectFromId(pesquisa_id, this.pesquisa) == null)
                return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dataSource.close();
        return true;
    }
}
