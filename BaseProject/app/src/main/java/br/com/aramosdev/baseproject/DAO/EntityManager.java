package br.com.aramosdev.baseproject.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.List;

import br.com.aramosdev.baseproject.annotation.Autoincrement;
import br.com.aramosdev.baseproject.annotation.ConfigDatabase;
import br.com.aramosdev.baseproject.annotation.Field;
import br.com.aramosdev.baseproject.annotation.Id;
import br.com.aramosdev.baseproject.annotation.Table;


/**
 * @author Alberto Ramos
 * Created by programador on 26/01/15.
 */
public class EntityManager extends SQLiteOpenHelper {

    public StringBuffer sql = new StringBuffer();

    public EntityManager(Context context) {
        super(context, ConfigDatabase.name, null, ConfigDatabase.version);
    }
    @Override
    public void onCreate(SQLiteDatabase database) {
        this.sql = new StringBuffer();
        List<Class> classes = ConfigDatabase.table;
        for (Class cls : classes){
            String table = this.getTable(cls);
            this.sql.append("CREATE TABLE ");
            this.sql.append(table).append(" (");
            for (java.lang.reflect.Field field: cls.getDeclaredFields()) {
                if (field.isAnnotationPresent(Field.class)) {
                    Field anotacao = field.getAnnotation(Field.class);
                    this.sql.append(anotacao.name()).append(" ").append(anotacao.type());
                    if (field.isAnnotationPresent(Id.class)){
                        Id id = field.getAnnotation(Id.class);
                        this.sql.append(id.property);
                    }
                    if (field.isAnnotationPresent(Autoincrement.class)){
                        Autoincrement autoincrement = field.getAnnotation(Autoincrement.class);
                        this.sql.append(autoincrement.property);
                    }
                    this.sql.append(", ");
                }
            }
            this.sql.deleteCharAt(this.sql.length() - 2);
            this.sql.append(");");
            database.execSQL(this.sql.toString());
            Log.d("Sql =",this.sql.toString());
            this.sql = new StringBuffer();
        }
    }
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        this.sql = new StringBuffer();
        List<Class> classes = ConfigDatabase.table;
        for (Class cls : classes) {
            String table = this.getTable(cls);
            this.sql.append("DROP TABLE IF EXISTS ").append(table).append(";");
            database.execSQL(this.sql.toString());
            Log.d("Sql =",this.sql.toString());
            this.sql = new StringBuffer();
        }
        onCreate(database);
    }
    public String getTable(Class cls) {
        try {
            for (java.lang.annotation.Annotation annotation : cls.getAnnotations()) {
                if (annotation instanceof Table) {
                    Table table = (Table) annotation;
                    return table.name();
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return "";
    }
}
