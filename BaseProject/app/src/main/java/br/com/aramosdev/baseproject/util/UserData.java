package br.com.aramosdev.baseproject.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.telephony.TelephonyManager;

import java.util.UUID;

/**
 * @author Alberto Ramos
 * Created by programador on 26/01/15.
 */
public class UserData {
    public static String getEmail(Context context){
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null)
            return null;
        return account.name;
    }
    public static String getNumSerialCel(Context context){
        String IMEI = "";

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + telephonyManager.getDeviceId();
        tmSerial = "" + telephonyManager.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        IMEI = deviceUuid.toString();

        return IMEI;
    }
    private static Account getAccount(AccountManager accountManager){
//        Account[] accounts = accountManager.getAccountsByType("com.google");
//        Account account= null;
//        if (accounts.length >0)
//            account = accounts[0];
//        return account;
        return null;
    }
}
