package br.com.aramosdev.baseproject.annotation;

import java.util.ArrayList;
import java.util.List;

import br.com.aramosdev.baseproject.entity.Pesquisas;
import br.com.aramosdev.baseproject.entity.RespostaPesquisa;
import br.com.aramosdev.baseproject.entity.User;


/**
 * Created by programador on 27/01/15.
 */
public @interface ConfigDatabase {
    int version = 28;
    String name = "Opineonline";
    List<Class> table = new ArrayList<Class>(){{
        add(User.class);
        add(Pesquisas.class);
        add(RespostaPesquisa.class);
    }};
}
