package br.com.aramosdev.baseproject.util;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;

import br.com.aramosdev.baseproject.R;


/**
 * Created by programador on 18/03/15.
 */
public abstract class ConnectionInternet {
    private static ConnectivityManager conManager;

    public static boolean isWifi(Context cont){
        ConnectionInternet.conManager = (ConnectivityManager) cont.getSystemService(Context.CONNECTIVITY_SERVICE);
        return ConnectionInternet.conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }
    public static boolean isMobile3g(Context cont){
        ConnectionInternet.conManager = (ConnectivityManager) cont.getSystemService(Context.CONNECTIVITY_SERVICE);
        return ConnectionInternet.conManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
    }
    public static boolean isConnection(Context cont){
        ConnectionInternet.conManager = (ConnectivityManager) cont.getSystemService(Context.CONNECTIVITY_SERVICE);
        return  ConnectionInternet.conManager.getActiveNetworkInfo() != null
                &&  ConnectionInternet.conManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }
    public static void alertNotConnection(Context activity){
        AlertDialog.Builder alerta = new AlertDialog.Builder(activity);
        alerta.setTitle(activity.getString(R.string.title_connection));
        alerta.setMessage(activity.getString(R.string.message_not_connection));
        alerta.show();
    }
}
