package br.com.aramosdev.baseproject.entity;

import java.io.Serializable;

import br.com.aramosdev.baseproject.annotation.Autoincrement;
import br.com.aramosdev.baseproject.annotation.Field;
import br.com.aramosdev.baseproject.annotation.FieldsRequired;
import br.com.aramosdev.baseproject.annotation.Id;
import br.com.aramosdev.baseproject.annotation.Table;

/**
 * @author Alberto Ramos
 * Created by programador on 04/03/15.
 */
@Table(name="user")
public class User implements Serializable {
    @Id
    @Autoincrement
    @Field(name="id_user",type = "integer")
    private int id_user;
    @Field(name="login",type = "text")
    @FieldsRequired
    private String login;
    @Field(name="senha",type = "text")
    @FieldsRequired
    private String senha;
    private String imei;
    @Field(name="token",type = "text")
    private String token;
    @Field(name="status",type = "integer")
    private int status;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

}
